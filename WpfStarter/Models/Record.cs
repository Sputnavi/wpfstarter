﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace WpfStarter
{
    public class Record : INotifyPropertyChanged
    {
        [XmlAttribute]
        public int Id { get; set; }
        
        private DateTime? _date;
        public DateTime? Date { get => _date; set { _date = value; OnPropertyChanged(nameof(Date)); } }

        private string _firstName;
        public string FirstName { get => _firstName; set { _firstName = value; OnPropertyChanged(nameof(FirstName)); } }

        private string _lastName;
        public string LastName { get => _lastName; set { _lastName = value; OnPropertyChanged(nameof(LastName)); } }

        private string _surName;
        public string SurName { get => _surName; set { _surName = value; OnPropertyChanged(nameof(SurName)); } }

        private string _city;
        public string City { get => _city; set { _city = value; OnPropertyChanged(nameof(City)); } }

        private string _country;
        public string Country { get => _country; set { _country = value; OnPropertyChanged(nameof(City)); } }

        public Record()
        {
            FirstName = String.Empty;
            LastName = String.Empty;
            SurName = String.Empty;
            City = String.Empty;
            Country = String.Empty;
        }

        public Record(DateTime? date, string firstName, string lastName, string surName, string city, string country)
        {
            Date = date;
            FirstName = firstName;
            LastName = lastName;
            SurName = surName;
            City = city;
            Country = country;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
