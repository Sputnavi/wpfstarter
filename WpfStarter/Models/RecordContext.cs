﻿using System.Data.Entity;

namespace WpfStarter
{
    class RecordContext : DbContext
    {
        public RecordContext() : base("DBConnection")
        {

        }

        public DbSet<Record> Records { get; set; }
    }
}
