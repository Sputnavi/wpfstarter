﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using WpfStarter.Commands;
using WpfStarter.Services;
using WpfStarter.Services.Dialogs;
using WpfStarter.Services.Export;
using WpfStarter.Services.Import;
using WpfStarter.Services.Repositories;

namespace WpfStarter.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Record> _recordsCollection;
        private Record _searchRecord;
        private IRepository _repository;
        private IImporter _importer;
        private IExporter _exporter;
        private IDialogService _dialogService;
        private SearchEngine _searchEngine;

        
        public ObservableCollection<Record> RecordsCollection 
        { 
            get => _recordsCollection;
            set { _recordsCollection = value; OnPropertyChanged(nameof(RecordsCollection)); } 
        }

        public IExporter Exporter
        {
            get => _exporter;
            set { _exporter = value; OnPropertyChanged(nameof(Exporter)); }
        }

        public Record SearchRecord 
        { 
            get => _searchRecord;
            set { _searchRecord = value; OnPropertyChanged(nameof(SearchRecord)); } 
        }

        public MainViewModel()
        {
            _repository = new EFRecordRepository();
            RecordsCollection = new ObservableCollection<Record>(_repository.GetRecords());
            Exporter = new XmlExporter();
            _importer = new CsvImporter();
            _dialogService = new DefaultDialogService();
            _searchEngine = new SearchEngine();
            _searchRecord = new Record();
        }


        private RelayCommand _exportCommand;
        public RelayCommand ExportCommand
        {
            get => _exportCommand ??
                (_exportCommand = new RelayCommand(obj =>
                {
                    Exporter.Export(new List<Record>(RecordsCollection));
                    _dialogService.ShowMessage("Exported succesfully");
                }));
        }

        private RelayCommand _importCommand;
        public RelayCommand ImportCommand
        {
            get => _importCommand ??
                (_importCommand = new RelayCommand(obj =>
                {
                    if (_dialogService.OpenFileDialog("CSV files|*.csv") == false)
                        return;

                    string filePath = _dialogService.FilePath;
                    
                    var records = _importer.Import(filePath);
                    foreach (Record record in records)
                    {
                        _repository.Create(record);
                        RecordsCollection.Add(record);
                    }
                    _repository.Save();
                    
                    _dialogService.ShowMessage("Imported succesfully");
                }));
        }

        private RelayCommand _searchCommand;
        public RelayCommand SearchCommand
        {
            get => _searchCommand ??
                (_searchCommand = new RelayCommand(obj =>
                {
                    var searchResult = _searchEngine.Search(new RecordContext().Records, SearchRecord);
                    RecordsCollection = new ObservableCollection<Record>(searchResult);
                }));
        }

        private RelayCommand _clearCommand;
        public RelayCommand ClearCommand
        {
            get => _clearCommand ??
                (_clearCommand = new RelayCommand(obj =>
                {
                    SearchRecord = new Record();
                }));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
