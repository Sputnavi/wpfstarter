﻿using System.Collections.Generic;

namespace WpfStarter.Services.Import
{
    interface IImporter
    {
        List<Record> Import(string filePath);
    }
}
