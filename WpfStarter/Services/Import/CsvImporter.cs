﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WpfStarter.Services.Import
{
    class CsvImporter : IImporter
    {
        public List<Record> Import(string filePath)
        {
            var records = new List<Record>();
            using (var reader = new StreamReader(filePath, Encoding.UTF8))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var fields = line.Split(';');
                    var date = Convert.ToDateTime(fields[0]);
                    var firstName = fields[1];
                    var lastName = fields[2];
                    var surName = fields[3];
                    var city = fields[4];
                    var country = fields[5];

                    Record record = new Record(date, firstName, lastName, surName, city, country);
                    records.Add(record);
                }
            }
            return records;
        }
    }
}
