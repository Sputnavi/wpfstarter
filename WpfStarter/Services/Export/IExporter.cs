﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfStarter.Services.Export
{
    public interface IExporter
    {
        void Export(List<Record> records);
    }
}
