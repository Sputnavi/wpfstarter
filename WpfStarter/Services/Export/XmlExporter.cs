﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace WpfStarter.Services.Export
{
    class XmlExporter : IExporter
    {
        public void Export(List<Record> records)
        {
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Records.xml";
            var serializer = new XmlSerializer(typeof(List<Record>), new XmlRootAttribute("TestProgram"));
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(fileStream, records);
            }
        }
    }
}
