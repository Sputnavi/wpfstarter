﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace WpfStarter.Services.Export
{
    class ExcelExporter : IExporter
    {
        public void Export(List<Record> records)
        {
            Excel.Application app = new Excel.Application();
            Excel.Workbook workbook;
            Excel.Worksheet worksheet;

            object misValue = System.Reflection.Missing.Value;

            workbook = app.Workbooks.Add(misValue);
            worksheet = (Excel.Worksheet)workbook.Worksheets.get_Item(1);

            worksheet.Cells[1, 1] = "Date";
            worksheet.Cells[1, 2] = "First Name";
            worksheet.Cells[1, 3] = "Last Name";
            worksheet.Cells[1, 4] = "Surname";
            worksheet.Cells[1, 5] = "City";
            worksheet.Cells[1, 6] = "Country";

            for (int i = 0; i < records.Count; i++)
            {
                // Excel numbering starts from 1 and also we need skip headers
                int k = i + 2;
                worksheet.Cells[k, 1] = records[i].Date;  
                worksheet.Cells[k, 2] = records[i].FirstName;  
                worksheet.Cells[k, 3] = records[i].LastName;  
                worksheet.Cells[k, 4] = records[i].SurName;  
                worksheet.Cells[k, 5] = records[i].City;  
                worksheet.Cells[k, 6] = records[i].Country;  
            }

            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Records.xls";
            workbook.SaveAs(path, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            workbook.Close(true, misValue, misValue);
            app.Quit();

            Marshal.ReleaseComObject(worksheet);
            Marshal.ReleaseComObject(workbook);
            Marshal.ReleaseComObject(app);
        }
    }
}
