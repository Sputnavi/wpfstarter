﻿using System;
using System.Collections.Generic;

namespace WpfStarter.Services.Repositories
{
    interface IRepository : IDisposable
    {
        IEnumerable<Record> GetRecords();
        Record GetRecord(int Id);
        void Create(Record record);
        void Update(Record record);
        void Delete(int id);
        void Save();
    }
}
