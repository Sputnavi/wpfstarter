﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace WpfStarter.Services.Repositories
{
    class EFRecordRepository : IRepository
    {
        private RecordContext _db;

        public EFRecordRepository()
        {
            _db = new RecordContext();
        }

        public void Create(Record record)
        {
            _db.Records.Add(record);
        }

        public Record GetRecord(int id)
        {
            return _db.Records.Find(id);
        }
        public void Delete(int id)
        {
            var record = GetRecord(id);
            if (record != null)
                _db.Records.Remove(record);
        }

        public IEnumerable<Record> GetRecords()
        {
            return _db.Records.ToList();
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public void Update(Record record)
        {
            _db.Entry(record).State = EntityState.Modified;
        }

        private bool _disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
