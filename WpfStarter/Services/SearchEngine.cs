﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace WpfStarter.Services
{
    public class SearchEngine
    {
        public IEnumerable<Record> Search(DbSet<Record> records, Record searchRecord)
        {
            PrepareRecord(searchRecord);
            IQueryable<Record> search = records;
            if (searchRecord.Date.HasValue)
            {
                search = search.Where(q => q.Date == searchRecord.Date);
            }
            if (searchRecord.FirstName != "")
            {
                search = search.Where(q => q.FirstName == searchRecord.FirstName);
            }
            if (searchRecord.LastName != "")
            {
                search = search.Where(q => q.LastName == searchRecord.LastName);
            }
            if (searchRecord.SurName != "")
            {
                search = search.Where(q => q.SurName == searchRecord.SurName);
            }
            if (searchRecord.City != "")
            {
                search = search.Where(q => q.City == searchRecord.City);
            }
            if (searchRecord.Country != "")
            {
                search = search.Where(q => q.Country == searchRecord.Country);
            }

            return search.ToArray();
        }

        private void PrepareRecord(Record record)
        {
            record.FirstName = record.FirstName.Trim();
            record.LastName = record.LastName.Trim();
            record.SurName = record.SurName.Trim();
            record.City = record.City.Trim();
            record.Country = record.Country.Trim();
        }
    }
}
