﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfStarter.Services.Dialogs
{
    interface IDialogService
    {
        string FilePath { get; set; }
        bool OpenFileDialog(string filter);
        bool SaveFileDialog(string filter);
        void ShowMessage(string message);

    }
}
