﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using WpfStarter.Services.Export;

namespace WpfStarter.Services.Converters
{
    public class RadioBoolToExportConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is XmlExporter && parameter.ToString() == "XML")
                return true;
            if (value is ExcelExporter && parameter.ToString() == "Excel")
                return true;
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter.ToString() == "XML")
                return new XmlExporter();
            if (parameter.ToString() == "Excel")
                return new ExcelExporter();
            return new Exception("Wrong export parameter");
        }
    }
}
